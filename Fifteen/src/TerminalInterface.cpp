#include "TerminalInterface.h"

TerminalInterface::~TerminalInterface() {}

void TerminalInterface::askUserForBoardSetup() {
    cout << "Prosze o podanie kolejnych pol ukladu planszy:" << endl;
}

void TerminalInterface::showFieldNumber(int field) {
    cout << field + 1 << ": ";
}

int TerminalInterface::getFieldFromUser() {
    int field;

    cin >> field;
    return field;
}

void TerminalInterface::showBoardSetup(Board board) {
    for( int i = 0; i < board.getSize(); i += board.getWidth() ) {
        for(int j = 0; j < board.getWidth(); ++j) {
            cout << board.fields[i + j] << "   ";
        }
        cout << endl;
    }
}
