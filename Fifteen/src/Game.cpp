#include "Game.h"

#include <iostream>
#include <stdlib.h>
using std::size_t;


Game::Game() {
    interface = new TerminalInterface();
    startingBoard = new Board(4,4);
    mainBoard = new Board(4,4);
}
Game::~Game() {
    delete interface;
    delete mainBoard;
    delete startingBoard;
}

void Game::start() {
    //interface->askUserForBoardSetup();
    //setStartingFieldsFromUser();

//    startingBoard->fields.push_back(16.0);
//    startingBoard->fields.push_back(1.0);
//    startingBoard->fields.push_back(2.0);
//    startingBoard->fields.push_back(7.0);
//
//    startingBoard->fields.push_back(8.0);
//    startingBoard->fields.push_back(9.0);
//    startingBoard->fields.push_back(12.0);
//    startingBoard->fields.push_back(10.0);
//
//    startingBoard->fields.push_back(13.0);
//    startingBoard->fields.push_back(3.0);
//    startingBoard->fields.push_back(6.0);
//    startingBoard->fields.push_back(4.0);
//
//    startingBoard->fields.push_back(15.0);
//    startingBoard->fields.push_back(14.0);
//    startingBoard->fields.push_back(11.0);
//    startingBoard->fields.push_back(5.0);
    startingBoard->fields.push_back(6.0);
    startingBoard->fields.push_back(3.0);
    startingBoard->fields.push_back(1.0);
    startingBoard->fields.push_back(2.0);
    startingBoard->fields.push_back(9.0);
    startingBoard->fields.push_back(7.0);
    startingBoard->fields.push_back(8.0);
    startingBoard->fields.push_back(5.0);
    startingBoard->fields.push_back(4.0);



    mainBoard->copyFieldsFromBoard(startingBoard);
    mainBoard->setId( convertFieldsTableToBoardId(mainBoard->fields) );
    visitedStates.push_back( mainBoard->getId() );

    //inwardAlgorithm(mainBoard);
    //aStarAlgorithm(mainBoard);
    //playManually(mainBoard);
    recursiveBFS();

}

void Game::setStartingFieldsFromUser() {
    for(int i = 0; i < startingBoard->getSize(); ++i) {
        interface->showFieldNumber(i);
        startingBoard->fields.push_back( interface->getFieldFromUser() );
    }
 }

void Game::setDefaultFields() {
    startingBoard->fields.push_back(1);
    startingBoard->fields.push_back(2);
    startingBoard->fields.push_back(3);
    startingBoard->fields.push_back(4);
    startingBoard->fields.push_back(5);
    startingBoard->fields.push_back(16);
    startingBoard->fields.push_back(6);
    startingBoard->fields.push_back(8);
    startingBoard->fields.push_back(9);
    startingBoard->fields.push_back(11);
    startingBoard->fields.push_back(7);
    startingBoard->fields.push_back(12);
    startingBoard->fields.push_back(13);
    startingBoard->fields.push_back(10);
    startingBoard->fields.push_back(14);
    startingBoard->fields.push_back(15);

    cout << "Default starting board: " << endl;
    for (int i = 0; i < startingBoard->getSize(); ++i) {
        interface->showFieldNumber(i);
        cout << startingBoard->fields[i] << endl;
    }
}

 double Game::convertFieldsTableToBoardId(vector<int> fieldsTable) {
    int numberOfLowerStates;
    double id = 0.0;

    for(size_t i = 0; i < fieldsTable.size(); ++i) {
        numberOfLowerStates = 0;

        for(size_t j = 0 + i; j < fieldsTable.size(); ++j) {
            if(fieldsTable[i] > fieldsTable[j]) {
                ++numberOfLowerStates;
            }
        }
        id += numberOfLowerStates * ( math.strong(fieldsTable.size() - i - 1) );
    }
    return id;
}

void Game::inwardAlgorithm(Board board) {
    cout.precision(17);
    direction actualDirection = UP;
    direction firstDirection = UP;
    direction secondDirection = LEFT;
    direction thirdDirection = DOWN;
    direction fourthDirection = RIGHT;
    Board bufferBoard = board;

    cout << board.getId() << endl;
    cout << bufferBoard.getId() << endl;

    board.setId( convertFieldsTableToBoardId(board.fields) );

    if( isFinished( board.getId() ) != true ) {
        while(true) {
            if( isPossibleToMove( firstDirection, &board ) ) {
                bufferBoard = board;
                moveBoardTileUp(&bufferBoard);

                if( isFinished( bufferBoard.getId() ) == true ) {
                    cout << "koniec!" << endl;
                    return;
                }

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    board = bufferBoard;
                    visitedStates.push_back( board.getId() );
                    interface->showBoardSetup(board);
                    continue;
                }
                else {
                    directionsStack.pop();
                }
            }
            if( isPossibleToMove( secondDirection, &board ) ) {
                bufferBoard = board;
                moveBoardTileLeft(&bufferBoard);
                cout << "lewo" << endl;

                if( isFinished( bufferBoard.getId() ) == true ) {
                    cout << "koniec!" << endl;
                    return;
                }

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    board = bufferBoard;
                    visitedStates.push_back( board.getId() );
                    interface->showBoardSetup(board);
                    continue;
                }
                else {
                    directionsStack.pop();
                }
            }
            if( isPossibleToMove( thirdDirection, &board ) ) {
                bufferBoard = board;
                moveBoardTileDown(&bufferBoard);
                cout << "dol" << endl;

                if( isFinished( bufferBoard.getId() ) == true ) {
                    cout << "koniec!" << endl;
                    return;
                }

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    board = bufferBoard;
                    visitedStates.push_back( board.getId() );
                    interface->showBoardSetup(board);
                    continue;
                }
                else {
                    directionsStack.pop();
                }
            }
            if( isPossibleToMove( fourthDirection, &board ) ) {
                bufferBoard = board;
                moveBoardTileRight(&bufferBoard);
                cout << "prawo" << endl;

                if( isFinished( bufferBoard.getId() ) == true ) {
                    cout << "koniec!" << endl;
                    return;
                }

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    board = bufferBoard;
                    visitedStates.push_back( board.getId() );
                    interface->showBoardSetup(board);
                    continue;
                }
                else {
                    directionsStack.pop();
                }
            }

            actualDirection = directionsStack.top();
            directionsStack.pop();

            switch(actualDirection) {
            case UP:
                board.moveTileDown();
                break;
            case DOWN:
                board.moveTileUp();
                break;
            case LEFT:
                board.moveTileRight();
                break;
            case RIGHT:
                board.moveTileLeft();
                break;
            }
        }
    }
}

void Game::aStarAlgorithm(Board board) {
    int numberOfMoves = 0;
    int weight;
    list<BoardTree*>::iterator lowestWeightIndex;
    BoardTree startNode(board, numberOfMoves);
    startNode.countAndSetWeight();
    BoardTree *head = &startNode;

    openStates.push_back(head);

    interface->showBoardSetup(startNode.board);

    while( !openStates.empty() ) {
        weight = 999;

        for(list<BoardTree*>::iterator i = openStates.begin(); i != openStates.end(); ++i) {
            if( (*i)->getWeight() < weight) {
                weight = (*i)->getWeight();
                lowestWeightIndex = i;
            }
        }
        head = *lowestWeightIndex;

        //cout <<"uklad o najmniejszej wadze " << head->getWeight() << " i liczbie ruchow: " << head->getNumberOfMoves() << endl;
        //interface->showBoardSetup(head->board);

        openStates.erase(lowestWeightIndex);

        if( isFinished( head->board.getId() ) ) {
            cout << "znaleziono!!" << endl;
            interface->showBoardSetup(head->board);
            break;
        }

        if( isPossibleToMove( UP, &(head->board) ) ) {
            head->up = new BoardTree(head->board, head->getNumberOfMoves() + 1);
            moveBoardTileUp(&head->up->board);
            if( aStarMove(head->up) ) {
                head->up = nullptr;
            }
        }

        if( isPossibleToMove( DOWN, &head->board ) ) {
            head->down = new BoardTree(head->board, head->getNumberOfMoves() + 1);
            moveBoardTileDown(&head->down->board);
            if( aStarMove(head->down) ) {
                head->down = nullptr;
            }
        }

        if( isPossibleToMove( LEFT, &head->board ) ) {
            head->left = new BoardTree(head->board, head->getNumberOfMoves() + 1);
            moveBoardTileLeft(&head->left->board);
            if( aStarMove(head->left) ) {
                head->left = nullptr;
            }
        }


        if( isPossibleToMove( RIGHT, &head->board ) ) {
            head->right = new BoardTree(head->board, head->getNumberOfMoves() + 1);
            moveBoardTileRight(&head->right->board);

            if( aStarMove(head->right) ) {
                head->right = nullptr;
            }
        }
        closeStates.push_back(head);
    }
}

bool Game::aStarMove(BoardTree *vertex) {
    bool isNulled = false;

    vertex->countAndSetWeight();
    for(list<BoardTree*>::iterator i = openStates.begin(); i != openStates.end(); ++i) {
        if( (*i)->board.getId() == vertex->board.getId() ) {
            if( (*i)->getWeight() < vertex->getWeight() ) {
                delete vertex;
                vertex = nullptr;
                isNulled = true;
                break;
            }
        }
    }

    if(vertex != nullptr) {
        if(!closeStates.empty()) {
            for(list<BoardTree*>::iterator i = closeStates.begin(); i != closeStates.end(); ++i) {
                if( (*i)->board.getId() == vertex->board.getId() ) {
                    if( (*i)->getWeight() < vertex->getWeight() ) {
                        delete vertex;
                        vertex = nullptr;
                        isNulled = true;
                        break;
                    }
                }
            }
        }
    }

    if(vertex != nullptr) {
        for(list<BoardTree*>::iterator i = openStates.begin(); i != openStates.end(); ++i) {
            if( (*i)->board.getId() == vertex->board.getId() ) {
                openStates.erase(i);
            }
        }

        for(list<BoardTree*>::iterator i = closeStates.begin(); i != closeStates.end(); ++i) {
            if( (*i)->board.getId() == vertex->board.getId() ) {
                closeStates.erase(i);
            }
        }
        openStates.push_back(vertex);
    }
    return isNulled;
}

bool Game::isPossibleToMove(direction directionToMove, Board *board) {
    switch(directionToMove) {
    case UP:
        if( board->getSocketIndex() < board->getWidth() * (board->getHeight() - 1) ) {
            return true;
        }
        break;
    case DOWN:
        if( board->getSocketIndex() > board->getWidth() - 1 ) {
            return true;
        }
        break;
    case LEFT:
        if( ( (board->getSocketIndex() + 1) % board->getWidth() ) != 0  ) {
            return true;
        }
        break;
    case RIGHT:
        if( board->getSocketIndex() % board->getWidth() != 0) {
            return true;
        }
        break;
    }
    return false;
}

void Game::moveBoardTileUp(Board *board ) {
    board->moveTileUp();
    board->setId( convertFieldsTableToBoardId(board->fields) );
    directionsStack.push(UP);
}

void Game::moveBoardTileDown(Board *board ) {
    board->moveTileDown();
    board->setId( convertFieldsTableToBoardId(board->fields) );
    directionsStack.push(DOWN);
}

void Game::moveBoardTileLeft(Board *board ) {
    board->moveTileLeft();
    board->setId( convertFieldsTableToBoardId(board->fields) );
    directionsStack.push(LEFT);
}

void Game::moveBoardTileRight(Board *board ) {
    board->moveTileRight();
    board->setId( convertFieldsTableToBoardId(board->fields) );
    directionsStack.push(RIGHT);
}

bool Game::isFinished(double boardId) {
    if(boardId == 0.0) {    // osiagniecie zera oznacza poprawnie posortowana plansze
        cout << "zero!" << endl;
        return true;
    }
    return false;
}

bool Game::isSetupWasBefore(double setupId) {
    for(size_t i = 0; i < visitedStates.size(); ++i) {
        if(visitedStates[i] == setupId) {
            return true;
        }
    }
    return false;
}

void Game::playManually(Board board) {
    Board bufferBoard = board;
    char key;

    while(true) {

        key = 'x';
        cin >> key;

        switch(key) {
        case 'g':
            if( board.getSocketIndex() < board.getWidth() * (board.getHeight() - 1) ) {
                bufferBoard = board;
                bufferBoard.moveTileUp();
                bufferBoard.setId( convertFieldsTableToBoardId(bufferBoard.fields) );

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    visitedStates.push_back( bufferBoard.getId() );
                    interface->showBoardSetup(bufferBoard);
                    board = bufferBoard;
                }
                else {
                    cout << "takie ustawienie juz bylo" << endl;
                }
            }
            else {
                cout << "nie mozna przesunac do gory" << endl;
            }
            break;
        case 'l':
            if( ( (board.getSocketIndex() + 1) % board.getWidth() ) != 0  ) {
                cout << "lewo" << endl;
                bufferBoard = board;
                bufferBoard.moveTileLeft();
                bufferBoard.setId( convertFieldsTableToBoardId(bufferBoard.fields) );

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {

                    visitedStates.push_back( bufferBoard.getId() );
                    interface->showBoardSetup(bufferBoard);
                    board = bufferBoard;
                }
                else {
                    cout << "takie ustawienie juz bylo" << endl;
                }
            }
            else {
                cout << "nie mozna przesunac w lewo" << endl;
            }
            break;
        case 'd':
            if( board.getSocketIndex() > board.getWidth() - 1 ) {
                bufferBoard = board;
                cout << "dol" << endl;
                bufferBoard.moveTileDown();
                bufferBoard.setId( convertFieldsTableToBoardId(bufferBoard.fields) );

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    visitedStates.push_back( bufferBoard.getId() );
                    interface->showBoardSetup(bufferBoard);
                    board = bufferBoard;
                }
                else {
                    cout << "takie ustawienie juz bylo" << endl;
                }
            }
            else {
                cout << "nie mozna przesunac w dol" << endl;
            }
            break;
        case 'p':
            if( ( board.getSocketIndex() ) % board.getWidth() != 0) {
                bufferBoard = board;
                cout << "prawo" << endl;
                bufferBoard.moveTileRight();
                bufferBoard.setId( convertFieldsTableToBoardId(bufferBoard.fields) );

                if( !isSetupWasBefore( bufferBoard.getId() ) ) {
                    visitedStates.push_back( bufferBoard.getId() );
                    interface->showBoardSetup(bufferBoard);
                    board = bufferBoard;
                }
                else {
                    cout << "takie ustawienie juz bylo" << endl;
                }
            }
            else {
                cout << "nie mozna przesunac w prawo" << endl;
            }
        }
    }
}

void Game::recursiveBFS() {
    int currentDepth = 0;
    while (!recursiveBFS(currentDepth)) {
        currentDepth++;
    }
    cout << "\n\n\nSolution: ";
    printSolution();
}

void Game::printSolution() {
    int movesNumber = directionVector.size();
    for (int i = 0; i < movesNumber; i++) {
        switch(directionVector[i]){
            case UP:
                cout << "UP ";
                break;
            case DOWN:
                cout << "DOWN ";
                break;
            case RIGHT:
                cout << "RIGHT ";
                break;
            case LEFT:
                cout << "LEFT ";
                break;
        }
    }
    cout << endl;
}

void Game::undoMove() {
//    direction moveDirection = directionsStack[directionsStack.size()-1];
    direction moveDirection = (direction) directionVector.back();
    directionVector.pop_back();
    switch (moveDirection) {
        case UP:
            mainBoard->moveTileDown();
            break;
        case DOWN:
            mainBoard->moveTileUp();
            break;
        case LEFT:
            mainBoard->moveTileRight();
            break;
        case RIGHT:
            mainBoard->moveTileLeft();
            break;
    }
}

bool Game::recursiveBFS(int depth) {
    if (depth == 0) {
        mainBoard->computeBoardId();
        if (isSetupWasBefore(mainBoard->getId())) { return false; }
        visitedStates.push_back(mainBoard->getId());
        return mainBoard->getId() == 0.0;
    }
    if (mainBoard->canMoveDown()) {
        directionVector.push_back(DOWN);
        mainBoard->moveTileDown();
        if (recursiveBFS(depth - 1)) {
            return true;
        } else {
            undoMove();
        }
    }
    if (mainBoard->canMoveRight()) {
        directionVector.push_back(RIGHT);
        mainBoard->moveTileRight();
        if (recursiveBFS(depth - 1)) {
            return true;
        } else {
            undoMove();
        }
    }
    if (mainBoard->canMoveUp()) {
        directionVector.push_back(UP);
        mainBoard->moveTileUp();
        if (recursiveBFS(depth - 1)) {
            return true;
        } else {
            undoMove();
        }
    }
    if (mainBoard->canMoveLeft()) {
        directionVector.push_back(LEFT);
        mainBoard->moveTileLeft();
        if (recursiveBFS(depth - 1)) {
            return true;
        } else {
            undoMove();
        }
    }
    return false;
}
