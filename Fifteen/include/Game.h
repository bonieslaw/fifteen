#ifndef GAME_H
#define GAME_H

#include <vector>
#include <list>
#include <stack>
#include "TerminalInterface.h"
#include "Board.h"
#include "BoardTree.h"
#include "Math.h"


using std::vector;
using std::list;
using std::stack;

class Game
{

private:
    Interface* interface;
    Board* mainBoard;
    Board* startingBoard;
    Math math;



public:
    enum direction {UP, DOWN, LEFT, RIGHT};
    Game();
    ~Game();
    void start();
    void setStartingFieldsFromUser();
    void setDefaultFields();
    double convertFieldsTableToBoardId(vector<int>);
    void inwardAlgorithm(Board);
    void aStarAlgorithm(Board);
    bool aStarMove(BoardTree*);
    bool isPossibleToMove(direction, Board*);
    void moveBoardTileUp(Board*);
    void moveBoardTileDown(Board*);
    void moveBoardTileLeft(Board*);
    void moveBoardTileRight(Board*);
    bool isFinished(double);
    bool isSetupWasBefore(double);
    void playManually(Board);

    void recursiveBFS();
    bool recursiveBFS(int depth);
    void printSolution();
    void undoMove();


    stack<direction> directionsStack;
    list<BoardTree*> openStates;
    list<BoardTree*> closeStates;
    vector<double> visitedStates;
    vector<direction> directionVector;

};

#endif // GAME_H
