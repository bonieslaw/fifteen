#include "BoardTree.h"

BoardTree::BoardTree(Board b, int numberOfMoves) : board(b) {
    this->numberOfMoves = numberOfMoves;
    countAndSetWeight();
    isExtended = false;
    up = nullptr;
    down = nullptr;
    left = nullptr;
    right = nullptr;
}

BoardTree::~BoardTree() {
    delete up;
    delete down;
    delete left;
    delete right;
}

void BoardTree::setIsExtended(bool isExtended) {
    this->isExtended = isExtended;
}

bool BoardTree::getIsExtended() {
    return isExtended;
}

int BoardTree::getWeight() {
    return weight;
}

int BoardTree::getNumberOfMoves() {
    return numberOfMoves;
}

//void BoardTree::countAndSetWeight() {
//    weight = board.countImproperTiles() + numberOfMoves;
//}

void BoardTree::countAndSetWeight() {
    int actuallRow;
    int tileGoalRow;
    int actuallColumn;
    int tileGoalColumn;
    int counter = 0;

    for(int i = 0; i < board.getSize(); ++i) {
        if(board.fields[i] != board.getSocketIndex()) {
            if(board.fields[i] != i+1) {
            actuallRow = i / board.getWidth();
            tileGoalRow = board.fields[i] - 1 / board.getWidth();

            if(actuallRow < tileGoalRow) {
                counter += tileGoalRow - actuallRow;
            }
            else {
                counter += actuallRow - tileGoalRow;
            }

            actuallColumn = i + board.getWidth() % board.getWidth();
            tileGoalColumn = (board.fields[i] - 1)  + board.getWidth() % board.getWidth();

            if(actuallColumn < tileGoalColumn) {
                counter += tileGoalColumn - actuallColumn;
            }
            else {
                counter += actuallColumn - tileGoalColumn;
            }
        }
    }
        }

    weight = counter + numberOfMoves;
}
