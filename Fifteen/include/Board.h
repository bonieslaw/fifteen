#ifndef BOARD_H
#define BOARD_H
#include <vector>
#include "Math.h"

#include <iostream>
using namespace std;

using std::vector;
using std::size_t;


class Board
{
private:
    double id;
    int width;
    int height;
    Math math;

public:
    vector<int> fields;

    Board(unsigned int, unsigned int);
    Board(Board*);
    Board& operator =(Board&);
    void setId(double);
    double getId();
    int getWidth();
    int getHeight();
    int getSize();
    int getSocketIndex();
    int countImproperTiles();
    void moveTileDown();
    void moveTileRight();
    void moveTileUp();
    void moveTileLeft();
    void copyFieldsFromBoard(Board*);
    bool canMoveUp();
    bool canMoveDown();
    bool canMoveLeft();
    bool canMoveRight();
    void computeBoardId();
};

#endif // BOARD_H
