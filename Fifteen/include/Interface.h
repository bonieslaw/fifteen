#ifndef INTERFACE_H
#define INTERFACE_H
#include <iostream>
#include "Board.h"

using std::string;


class Interface
{
public:
    virtual ~Interface() {};
    virtual void askUserForBoardSetup() = 0;
    virtual void showFieldNumber(int) = 0;
    virtual int getFieldFromUser() = 0;
    virtual void showBoardSetup(Board) = 0;
};

#endif // INTERFACE_H
