#ifndef BOARDTREE_H
#define BOARDTREE_H
#include "Board.h"


class BoardTree
{
private:
    int numberOfMoves;
    int weight;
    bool isExtended;
public:
    BoardTree(Board, int);
    ~BoardTree();
    Board board;
    BoardTree *up;
    BoardTree *down;
    BoardTree *left;
    BoardTree *right;

    void setIsExtended(bool);
    bool getIsExtended();
    int getWeight();
    int getNumberOfMoves();
    void countAndSetWeight();
};

#endif // BOARDTREE_H
