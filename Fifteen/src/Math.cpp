#include "Math.h"

double Math::strong(double number) {
    if(number == 0) {
        return 1.0;
    }
    else {
        return number * strong(number - 1);
    }
}
