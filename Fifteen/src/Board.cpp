#include "Board.h"

Board::Board(unsigned int w, unsigned int h)
:width(w), height(h) {
}

Board::Board(Board *board) {
    this->id = board->id;
    this->width = board->width;
    this->height = board->height;
    this->fields.clear();

    for(size_t i = 0; i < board->fields.size(); ++i) {
        this->fields.push_back(board->fields[i]);
    }
}

Board& Board::operator =(Board& board) {
    this->id = board.id;
    this->width = board.width;
    this->height = board.height;
    this->fields.clear();

    for(size_t i = 0; i < board.fields.size(); ++i) {
        this->fields.push_back(board.fields[i]);
    }
    return *this;
}

void Board::setId(double id) {
    this->id = id;
}

double Board::getId() {
    return id;
}

int Board::getSize() {
    return width * height;
}

int Board::getWidth() {
    return width;
}

int Board::getHeight() {
    return height;
}

int Board::getSocketIndex() {
    int socket = getSize(); //reprezentacja gniazda na planszy

    for(size_t i = 0; i < fields.size(); ++i) {
        if(fields[i] == socket) {
            return i;
        }
    }
    return 99;
}

int Board::countImproperTiles() {
    int number = 0;

    for(size_t i = 0; i < fields.size(); ++i) {
        if(fields[i] != i + 1) {
            ++number;
        }
    }
    return number;
}

void Board::moveTileDown() {
    int socketIndex = getSocketIndex();
    int tileToMoveIndex = socketIndex - width;
    int fieldBuffer;

    fieldBuffer = fields[socketIndex];
    fields[socketIndex] = fields[tileToMoveIndex];
    fields[tileToMoveIndex] = fieldBuffer;
}

void Board::moveTileRight() {
    int socketIndex = getSocketIndex();
    int tileToMoveIndex = socketIndex - 1;
    int fieldBuffer;

    fieldBuffer = fields[socketIndex];
    fields[socketIndex] = fields[tileToMoveIndex];
    fields[tileToMoveIndex] = fieldBuffer;
}

void Board::moveTileUp() {
    int socketIndex = getSocketIndex();
    int tileToMoveIndex = socketIndex + width;
    int fieldBuffer;

    fieldBuffer = fields[socketIndex];
    fields[socketIndex] = fields[tileToMoveIndex];
    fields[tileToMoveIndex] = fieldBuffer;
}

void Board::moveTileLeft() {
    int socketIndex = getSocketIndex();
    int tileToMoveIndex = socketIndex + 1;
    int fieldBuffer;

    fieldBuffer = fields[socketIndex];
    fields[socketIndex] = fields[tileToMoveIndex];
    fields[tileToMoveIndex] = fieldBuffer;
}

void Board::copyFieldsFromBoard(Board* board) {
    this->fields.clear();

    for(size_t i = 0; i < board->fields.size(); ++i) {
        this->fields.push_back(board->fields[i]);
    }
}

bool Board::canMoveUp() {
    return this->getSocketIndex() < this->getWidth() * (this->getHeight() - 1);
}

bool Board::canMoveDown() {
    return this->getSocketIndex() > this->getWidth() - 1;
}

bool Board::canMoveLeft() {
    return (this->getSocketIndex() + 1) % this->getWidth() != 0;
}

bool Board::canMoveRight() {
    return (this->getSocketIndex()) % this->getWidth() != 0;
}

void Board::computeBoardId() {
    int numberOfLowerStates;
    double id = 0.0;

    for (size_t i = 0; i < this->fields.size(); ++i) {
        numberOfLowerStates = 0;

        for (size_t j = 0 + i; j < this->fields.size(); ++j) {
            if (this->fields[i] > this->fields[j]) {
                ++numberOfLowerStates;
            }
        }
        id += numberOfLowerStates * math.strong(this->fields.size() - i - 1);
    }
    this->id = id;
}
