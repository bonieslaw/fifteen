#ifndef TERMINALINTERFACE_H
#define TERMINALINTERFACE_H

#include <Interface.h>

using std::cin;
using std::cout;
using std::endl;


class TerminalInterface : public Interface
{
public:
    ~TerminalInterface();
    void askUserForBoardSetup();
    void showFieldNumber(int);
    int getFieldFromUser();
    void showBoardSetup(Board);
};

#endif // TERMINALINTERFACE_H
