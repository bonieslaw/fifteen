#include "EmptyTile.h"

EmptyTile::EmptyTile()
{
    up = nullptr;
    down = nullptr;
    left = nullptr;
    right = nullptr;
}

void EmptyTile::setUp(Board* board) {
    this->up = board;
}

void EmptyTile::setDown(Board* board) {
    this->down = board;
}

void EmptyTile::setRight(Board* board) {
    this->right = board;
}

void EmptyTile::setLeft(Board* board) {
    this->left = board;
}

Board* EmptyTile::getUp() {
    return up;
}

Board* EmptyTile::getDown() {
    return down;
}

Board* EmptyTile::getLeft() {
    return left;
}

Board* EmptyTile::getRight() {
    return right;
}
